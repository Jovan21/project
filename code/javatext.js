// function toggleText(button) {
//     const textContainer = button.parentElement;
//     const additionalText = textContainer.getElementsByClassName("additional-text")[0];

//     if (additionalText.style.display === "none") {
//       additionalText.style.display = "block";
//       button.innerHTML = "Read Less";
//     } else {
//       additionalText.style.display = "none";
//       button.innerHTML = "Read More";
//     }
//   }
// console.log($)

function toggleText(button) {
  const $textContainer = $(button).parent();
  const $additionalText = $textContainer.find(".additional-text");

  if ($additionalText.is(":hidden")) {
    $additionalText.show();
    $(button).text("Read Less");
  } else {
    $additionalText.hide();
    $(button).text("Read More");
  }
}

// console.log($)

function createChart() {
  $("#chart").kendoChart({
    title: {
      text: "Total Sales",
    },
    subtitle: {
      text: "Year:2022",
    },
    legend: {
      visible: true,
    },
    seriesDefaults: {
      type: "line",
    },
    series: [
      {
        name: "Asus Laptops",
        data: [20, 63, 45, 98, 112, 19],
        color: "red",
      },
      {
        name: "Hp Laptops",
        data: [52, 31, 29, 90, 130, 165],
        color: "blue",
      },
    ],
    valueAxis: {
      max: 200,
      line: {
        visible: false,
      },
      minorGridLines: {
        visible: true,
      },
      labels: {
        rotation: "auto",
      },
    },
    categoryAxis: {
      categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
      majorGridLines: {
        visible: false,
      },
    },
    tooltip: {
      visible: true,
      template: "#= series.name #: #= value #",
    },
  });
}

$(document).ready(createChart);
$(document).bind("kendo:skinChange", createChart);

$(document).ready(function () {
  var dataSource = new kendo.data.DataSource({
    data: [
      { CountryID: 1, CountryName: "Macedonia" },
      { CountryID: 2, CountryName: "Serbia" },
      { CountryID: 3, CountryName: "Albania" },
      { CountryID: 4, CountryName: "Greece" },
      { CountryID: 5, CountryName: "Bulgaria" },
      { CountryID: 6, CountryName: "Croatia" },
      { CountryID: 7, CountryName: "Slovenia" },
      { CountryID: 8, CountryName: "Germany" },
      { CountryID: 9, CountryName: "England" },
      { CountryID: 10, CountryName: "Norway" },
      { CountryID: 11, CountryName: "Sweden" },
      { CountryID: 12, CountryName: "Finland" },
      { CountryID: 13, CountryName: "Russia" },
      { CountryID: 14, CountryName: "USA" },
      { CountryID: 15, CountryName: "Spain" },
      { CountryID: 16, CountryName: "Portugal" },
      { CountryID: 17, CountryName: "France" },
      { CountryID: 18, CountryName: "Italy" },
      { CountryID: 19, CountryName: "Mexico" },
      { CountryID: 20, CountryName: "Argentina" },
    ],
    sort: { field: "CountryName", dir: "asc" },
  });

  $("#kd-place-chooser").kendoDropDownList({
    filter: "contains",
    optionLabel: "Please select country...",
    dataTextField: "CountryName",
    dataValueField: "CountryID",
    dataSource: dataSource,
  });
});

$(function () {
  $("#form").submit(function (event) {
    event.preventDefault();

    var name = $("#name").val();
    var email = $("#email").val();
    var country = $("#kd-place-chooser").data("kendoDropDownList").text();
    var message = $("#message").val();

    var isValid = true;

    if (name === "") {
      isValid = false;
    }

    if (email === "") {
      isValid = false;
    }

    if (country === "") {
      isValid = false;
    }

    if (message === "") {
      isValid = false;
    }

    if (isValid) {
      alert("Form submitted successfully");
      $("#form")[0].reset();
    } else {
      alert("Please fill the form correctly");
    }
  });
});


function myFunction() {
  var element = document.body;
  element.classList.toggle("dark-mode");
}